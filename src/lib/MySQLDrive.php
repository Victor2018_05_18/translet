<?php
namespace lib;

class MySQLDrive
{
    private $connect;

    public function __construct($host, $user, $pass, $bd)
    {
            $this->connect = new \mysqli($host, $user, $pass, $bd);
            if(!$this->connect){
                echo 'ERROR';
            }
    }

    public function getConnect(){
        return $this->connect;
    }

}